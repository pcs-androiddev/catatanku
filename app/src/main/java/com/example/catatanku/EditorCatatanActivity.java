package com.example.catatanku;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.HashSet;

public class EditorCatatanActivity extends AppCompatActivity {

    int catatanID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_catatan);

        EditText editText = (EditText) findViewById(R.id.editText);

        Intent intent = getIntent();
        catatanID = intent.getIntExtra("catatanID", -1);

        if (catatanID != -1) {

            editText.setText(MainActivity.catatanku.get(catatanID));

        } else {

            MainActivity.catatanku.add("");
            catatanID = MainActivity.catatanku.size() - 1;
            MainActivity.arrayAdapter.notifyDataSetChanged();

        }


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                MainActivity.catatanku.set(catatanID, String.valueOf(charSequence));
                MainActivity.arrayAdapter.notifyDataSetChanged();

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("com.example.catatanku", Context.MODE_PRIVATE);

                HashSet<String> set = new HashSet(MainActivity.catatanku);

                sharedPreferences.edit().putStringSet("set", set).apply();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
}